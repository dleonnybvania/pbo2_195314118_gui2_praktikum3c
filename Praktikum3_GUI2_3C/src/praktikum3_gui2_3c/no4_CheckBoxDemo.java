/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package praktikum3_gui2_3c;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class no4_CheckBoxDemo extends JDialog {
    private JCheckBox cekBoxc, cekBoxb, cekBoxi;
       public no4_CheckBoxDemo(){
           setLayout(null);
           setTitle("CheckBoxDemo");
           setSize(700,500);
           setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
           setVisible(true);
           
           JButton butKiri = new JButton();
           JButton butKanan = new JButton();
           
           butKiri = new JButton("Left");
           butKiri.setBounds(223, 450, 65, 15);
           this.add(butKiri);
           butKanan = new JButton("Right");
           butKanan.setBounds(314, 450, 65, 15);
           this.add(butKanan);
           
        JCheckBox cekBoxc = new JCheckBox();
        cekBoxc = new JCheckBox("Centered");
        cekBoxc.setBounds(575, 3, 200, 50);
        this.add(cekBoxc);
        
        JCheckBox cekBoxb = new JCheckBox();
        cekBoxb = new JCheckBox("Bold");
        cekBoxb.setBounds(575, 15, 200, 50);
        this.add(cekBoxb);
        
        JCheckBox cekBoxi = new JCheckBox();
        cekBoxi = new JCheckBox("Italic");
        cekBoxi.setBounds(575, 27, 200, 50);
        this.add(cekBoxi);
        
            JTextArea textArea = new JTextArea();
            textArea.setText("Welcome to Java");
            textArea.setBounds(7, 7, 475, 75);
            textArea.setEditable(false);
            this.add(textArea);
            
        
       }
    
        public static void main(String[] args) {
            new no4_CheckBoxDemo();
        }
}
