/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package praktikum3_gui2_3c;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.net.URL;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class no3_TextnIconLabel extends JDialog {
    
    public no3_TextnIconLabel(){
        this.setLayout(null);
        setTitle("Text and Icon Label");
        setSize(700, 500);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setVisible(true);
        
        
        URL buah = this.getClass().getResource(Fruit.png);
        ImageIcon myFruit = new ImageIcon(buah);
        
        JLabel gambar = new JLabel();
        gambar.setIcon(myFruit);
        gambar.setBounds(119, 80, 185, 230);
        this.add(gambar);
        
        JLabel teks = new JLabel();
        teks = new JLabel("Jisoo");
        teks.setBounds(108, 55, 155, 200);
        this.add(teks);
        
    }
    public static void main(String[] args) {
        new no3_TextnIconLabel();
    }

}


