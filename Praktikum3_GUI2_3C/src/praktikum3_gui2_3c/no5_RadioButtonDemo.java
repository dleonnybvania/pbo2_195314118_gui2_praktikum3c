/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package praktikum3_gui2_3c;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;

/**
 *
 * @author SAMSUNG
 */
public class no5_RadioButtonDemo extends JDialog {
        private JCheckBox cekBoxc, cekBoxb, cekBoxi;
        private JRadioButton radioButred, radioButgreen, radioButblue;
       public no5_RadioButtonDemo(){
           setLayout(null);
           setTitle("CheckBoxDemo");
           setSize(700,500);
           setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
           setVisible(true);
           
           JButton butKiri = new JButton();
           JButton butKanan = new JButton();
           
           butKiri = new JButton("Left");
           butKiri.setBounds(223, 450, 65, 15);
           this.add(butKiri);
           butKanan = new JButton("Right");
           butKanan.setBounds(314, 450, 65, 15);
           this.add(butKanan);
           
        JCheckBox cekBoxc = new JCheckBox();
        cekBoxc = new JCheckBox("Centered");
        cekBoxc.setBounds(575, 3, 200, 50);
        this.add(cekBoxc);
        
        JCheckBox cekBoxb = new JCheckBox();
        cekBoxb = new JCheckBox("Bold");
        cekBoxb.setBounds(575, 15, 200, 50);
        this.add(cekBoxb);
        
        JCheckBox cekBoxi = new JCheckBox();
        cekBoxi = new JCheckBox("Italic");
        cekBoxi.setBounds(575, 27, 200, 50);
        this.add(cekBoxi);
        
            JTextArea textArea = new JTextArea();
            textArea.setText("Welcome to Java");
            textArea.setBounds(500, 500, 475, 75);
            textArea.setEditable(false);
            this.add(textArea);
            
            JRadioButton radioButred = new JRadioButton();
            radioButred.setText("Red");
            radioButred.setBounds(1, 45, 200, 50);
            this.add(radioButred);
            
            JRadioButton radioButgreen = new JRadioButton();
            radioButgreen.setText("Green");
            radioButgreen.setBounds(1, 50, 200, 50);
            this.add(radioButgreen);
            
            JRadioButton radioButblue = new JRadioButton();
            radioButblue.setText("Blue");
            radioButblue.setBounds(1, 55, 200, 50);
            this.add(radioButblue);
        
       }
    
        public static void main(String[] args) {
            new no5_RadioButtonDemo();
        }
}
