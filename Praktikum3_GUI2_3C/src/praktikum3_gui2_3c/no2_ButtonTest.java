/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package praktikum3_gui2_3c;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class no2_ButtonTest extends JDialog {
    
    public static void main(String[] args) {
        JFrame frame = new JFrame("ButtonTest");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(new Dimension(300,200));
                frame.setLocation(100,100);
                Container contentPane = frame.getContentPane();
		
		JDialog dlg = new JDialog (frame,"ButtonTest",true);
		dlg.setSize(200,100);
		dlg.show();

        JButton yel = new JButton("YELLOW");
        JButton blu = new JButton("BLUE");
        JButton red = new JButton("RED");
        
        JPanel panel = new JPanel();
        panel.add(yel);
        panel.add(blu);
        panel.add(red);

        
        contentPane.add(panel, BorderLayout.CENTER);

        frame.setVisible(true);
    }

}



